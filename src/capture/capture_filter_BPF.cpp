#include "capture_filter_BPF.h"

CaptureFilterBPF::CaptureFilterBPF()
{
    init();
};

void CaptureFilterBPF::init()
{
    filterObj = new pcpp::BpfFilterWrapper;
    filterString = nullptr;
}
/*
 */
bool CaptureFilterBPF::setFilter(const QString &filter)
{

    if (!filter.trimmed().isEmpty() &&
        filter != nullptr &&
        filterObj->setFilter(filter.trimmed().toStdString()))
    {
        /* filter compiles ok */
        filterString = filter;
        return true;
    }
    filterString = nullptr;
    return false;
}

QString CaptureFilterBPF::getFilter() const
{
    if (filterString != nullptr)
    {
        return filterString;
    }
    return nullptr;
}

bool CaptureFilterBPF::matchPacketvsFilter(const pcpp::RawPacket *rawPacket)
{
    if (filterString != nullptr && filterObj->matchPacketWithFilter(rawPacket))
        return true;
    return false;
}
