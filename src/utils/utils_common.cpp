#include "utils_common.h"

#include <QCoreApplication>
#include <QDebug>
// #include <QHash>
// #include <QString>
namespace cmm
{
    fileFormat getFileFormat(const QString &fileext)
    {
        QHashIterator<fileFormat, QString> iter(fileExtensions);
        while (iter.hasNext())
        {
            iter.next();
            if (iter.value() == fileext)
            {
                return iter.key();
            }
        }
        return fileFormat::pcapng;
    }

    QString getFileExtension(cmm::fileFormat fileFormatType)
    {

        if (fileExtensions.contains(fileFormatType))
        {
            return fileExtensions.value(fileFormatType);
        }
        return nullptr;
    }
}