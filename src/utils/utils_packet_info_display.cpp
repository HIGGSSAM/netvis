#include "utils_packet_info_display.h"

namespace pkl
{

    void addRoot(QTreeWidget *packetInfo, QTreeWidgetItem *layerRoot, QString data)
    {
        layerRoot->setText(0, data);
        packetInfo->addTopLevelItem(layerRoot);
    }

    void addSubRoot(QTreeWidgetItem *layerRoot, QTreeWidgetItem *subRoot, QString data)
    {
        subRoot->setText(0, data);
        layerRoot->addChild(subRoot);
    }

    void addChild(QTreeWidgetItem *parent, QString label, QString data)
    {
        QTreeWidgetItem *itm = new QTreeWidgetItem();
        itm->setText(0, label + data);
        parent->addChild(itm);
    }
}