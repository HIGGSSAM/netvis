#include "packet_layer_application.h"

namespace pkl
{
    void setDNSData(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::DNS))
        {
            pcpp::DnsLayer *dnsLayer = packet.getLayerOfType<pcpp::DnsLayer>();
            if (dnsLayer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find DNS layer";
            }
            else
            {
                auto *layerRoot = new QTreeWidgetItem(packetInfo);

                addRoot(packetInfo, layerRoot, QString::fromStdString("Domain Name System"));
                addChild(layerRoot, QString("Transation ID: "), QString::number(dnsLayer->getDnsHeader()->transactionID));

                auto *flagRoot = new QTreeWidgetItem(layerRoot);
                addSubRoot(layerRoot, flagRoot, QString("Flags: "));

                getDNSQueryOrResponse(dnsLayer->getDnsHeader()->queryOrResponse, flagRoot, dnsLayer);
                getDNSOpcode(dnsLayer->getDnsHeader()->opcode, flagRoot, dnsLayer);
                getDNSAuthoriative(dnsLayer->getDnsHeader()->authoritativeAnswer, flagRoot, dnsLayer);
                getDNSTruncation(dnsLayer->getDnsHeader()->truncation, flagRoot, dnsLayer);
                getDNSRecursionDesired(dnsLayer->getDnsHeader()->recursionDesired, flagRoot, dnsLayer);
                getDNSRecursionAvailable(dnsLayer->getDnsHeader()->recursionAvailable, flagRoot, dnsLayer);
                getDNSZero(dnsLayer->getDnsHeader()->zero, flagRoot, dnsLayer);
                getDNSAnswerAuthenticate(dnsLayer->getDnsHeader()->authoritativeAnswer, flagRoot, dnsLayer);
                getDNSNonAuthData(dnsLayer->getDnsHeader()->authenticData, flagRoot, dnsLayer);
                getDNSReplyCode(dnsLayer->getDnsHeader()->responseCode, flagRoot, dnsLayer);

                addChild(layerRoot, QString("Questions: "), QString::number((dnsLayer->getDnsHeader()->numberOfQuestions)));
                addChild(layerRoot, QString("Answers RRs: "), QString::number(dnsLayer->getDnsHeader()->numberOfAnswers));
                addChild(layerRoot, QString("Authority RRs: "), QString::number(dnsLayer->getDnsHeader()->numberOfAuthority));
                addChild(layerRoot, QString("Additional RRs: "), QString::number(dnsLayer->getDnsHeader()->numberOfAdditional));

                /* DNS Queries */
                if (dnsLayer->getFirstQuery() != nullptr)
                {
                    auto *queriesRoot = new QTreeWidgetItem(layerRoot);
                    addSubRoot(layerRoot, queriesRoot, QString("Queries"));
                    for (pcpp::DnsQuery *query = dnsLayer->getFirstQuery(); query != nullptr; query = dnsLayer->getNextQuery(query))
                    {
                        std::string queryName = query->getName();
                        std::string queryType = getDNSType(query->getDnsType());
                        std::string queryClass = getDNSClass(query->getDnsClass());
                        std::string queryLength = std::to_string(query->getSize());

                        auto *queryX = new QTreeWidgetItem(queriesRoot);
                        addSubRoot(queriesRoot, queryX, QString::fromStdString(queryName + ": type " + queryType + ", class " + queryClass));

                        addChild(queryX, QString("Name: "), QString::fromStdString(queryName));
                        addChild(queryX, QString("Type: "), QString::fromStdString(queryType));
                        addChild(queryX, QString("Class: "), QString::fromStdString(queryClass));
                        addChild(queryX, QString("Length: "), QString::fromStdString(queryLength));
                    }
                }

                /* DNS Answers */
                if (dnsLayer->getFirstAnswer() != nullptr)
                {
                    auto *answerRoot = new QTreeWidgetItem(layerRoot);
                    addSubRoot(layerRoot, answerRoot, QString("Answers"));
                    for (pcpp::DnsResource *answer = dnsLayer->getFirstAnswer(); answer != nullptr; answer = dnsLayer->getNextAnswer(answer))
                    {
                        std::string answerName = answer->getName();
                        std::string answerType = getDNSType(answer->getDnsType());
                        std::string answerClass = getDNSClass(answer->getDnsClass());
                        std::string answerLength = std::to_string(answer->getSize());
                        std::string answerTTL = std::to_string(answer->getTTL());
                        std::string answerAddress = answer->getData()->toString();

                        auto *answerX = new QTreeWidgetItem(answerRoot);
                        addSubRoot(answerRoot, answerX, QString::fromStdString(answerName + ": type " + answerType + ", class " + answerClass + ", addr " + answerAddress));

                        addChild(answerX, QString("Name: "), QString::fromStdString(answerName));
                        addChild(answerX, QString("Type: "), QString::fromStdString(answerType));
                        addChild(answerX, QString("Class: "), QString::fromStdString(answerClass));
                        addChild(answerX, QString("Time to live: "), QString::fromStdString(answerTTL));
                        addChild(answerX, QString("Data Length: "), QString::fromStdString(answerLength));
                        addChild(answerX, QString("Address: "), QString::fromStdString(answerAddress));
                    }
                }

                /* DNS Anthoritative Nameservers */
                if (dnsLayer->getFirstAuthority() != nullptr)
                {
                    auto *authoritativeRoot = new QTreeWidgetItem(layerRoot);
                    addSubRoot(layerRoot, authoritativeRoot, QString("Authoritative nameservers"));

                    for (pcpp::DnsResource *author = dnsLayer->getFirstAuthority(); author != nullptr; author = dnsLayer->getNextAuthority(author))
                    {
                        std::string authorName = author->getName();
                        std::string authorType = getDNSType(author->getDnsType());
                        std::string authorClass = getDNSClass(author->getDnsClass());
                        std::string authorTTL = std::to_string(author->getTTL());
                        std::string authorLength = std::to_string(author->getSize());

                        auto *authorX = new QTreeWidgetItem(authoritativeRoot);
                        addSubRoot(authoritativeRoot, authorX, QString::fromStdString(authorName + ": " + authorType + ", " + authorClass));

                        addChild(authorX, QString("Name: "), QString::fromStdString(authorName));
                        addChild(authorX, QString("Type: "), QString::fromStdString(authorType));
                        addChild(authorX, QString("Class: "), QString::fromStdString(authorClass));
                        addChild(authorX, QString("Time to live: "), QString::fromStdString(authorTTL));
                        addChild(authorX, QString("Data Length: "), QString::fromStdString(authorLength));
                        // TO ADD IN MORE DATA FROM RAW DATA
                    }
                }

                /* DNS Additional records */
                if (dnsLayer->getFirstAdditionalRecord() != nullptr)
                {
                    auto *additionalRoot = new QTreeWidgetItem(layerRoot);
                    addSubRoot(layerRoot, additionalRoot, QString("Additional records"));

                    for (pcpp::DnsResource *addRec = dnsLayer->getFirstAdditionalRecord(); addRec != nullptr; addRec = dnsLayer->getNextAdditionalRecord(addRec))
                    {
                        std::string addName = addRec->getName();
                        std::string addType = getDNSType(addRec->getDnsType());
                        std::string addClass = getDNSClass(addRec->getDnsClass());
                        std::string addTTL = std::to_string(addRec->getTTL());
                        std::string addLength = std::to_string(addRec->getSize());

                        auto *addX = new QTreeWidgetItem(additionalRoot);
                        addSubRoot(additionalRoot, addX, QString::fromStdString(addName + ": " + addType + ", " + addClass));

                        addChild(addX, QString("Name: "), QString::fromStdString(addName));
                        addChild(addX, QString("Type: "), QString::fromStdString(addType));
                        addChild(addX, QString("Class: "), QString::fromStdString(addClass));
                        addChild(addX, QString("Time to live: "), QString::fromStdString(addTTL));
                        addChild(addX, QString("Data Length: "), QString::fromStdString(addLength));
                        // TO ADD IN MORE DATA FROM RAW DATA
                    }
                }
            }
        }
    }

    void setHTTPRequestData(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::HTTPRequest))
        {
            pcpp::HttpRequestLayer const *httpRequestLayer = packet.getLayerOfType<pcpp::HttpRequestLayer>();
            if (httpRequestLayer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find HTTP request layer";
            }
            else
            {
                auto *layerRoot = new QTreeWidgetItem(packetInfo);

                std::string method = getHttpMethod(httpRequestLayer->getFirstLine()->getMethod());
                std::string version = getHttpVersion(httpRequestLayer->getFirstLine()->getVersion());

                addRoot(packetInfo, layerRoot, QString::fromStdString("Hypertext Transfer Protocol"));

                auto *methodLayer = new QTreeWidgetItem(layerRoot);
                addSubRoot(layerRoot, methodLayer, QString::fromStdString(method + " " + version));
                addChild(methodLayer, QString("Request Method: "), QString::fromStdString(method));
                addChild(methodLayer, QString("Request URI: "), QString::fromStdString(httpRequestLayer->getFirstLine()->getUri()));
                addChild(methodLayer, QString("Request Version: "), QString::fromStdString(version));

                for (pcpp::HeaderField *headerField = httpRequestLayer->getFirstField(); headerField != nullptr; headerField = httpRequestLayer->getNextField(headerField))
                {
                    addChild(layerRoot, QString::fromStdString(headerField->getFieldName() + ": "), QString::fromStdString(headerField->getFieldValue()));
                }
            }
        }
    }

    void setHTTPResponseData(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::HTTPResponse))
        {
            pcpp::HttpResponseLayer const *httpResponseLayer = packet.getLayerOfType<pcpp::HttpResponseLayer>();
            if (httpResponseLayer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find HTTP response layer";
            }
            else
            {
                auto *layerRoot = new QTreeWidgetItem(packetInfo);

                std::string version = getHttpVersion(httpResponseLayer->getFirstLine()->getVersion());
                std::string statusCode = std::to_string(httpResponseLayer->getFirstLine()->getStatusCodeAsInt());
                std::string responsePhrase = httpResponseLayer->getFirstLine()->getStatusCodeString();

                addRoot(packetInfo, layerRoot, QString::fromStdString("Hypertext Transfer Protocol"));

                auto *methodLayer = new QTreeWidgetItem(layerRoot);
                addSubRoot(layerRoot, methodLayer, QString::fromStdString(version + " " + statusCode + " " + responsePhrase));
                addChild(methodLayer, QString("Response Version: "), QString::fromStdString(version));
                addChild(methodLayer, QString("Status code: "), QString::fromStdString(statusCode));
                addChild(methodLayer, QString("Response Phrase: "), QString::fromStdString(responsePhrase));

                for (pcpp::HeaderField *headerField = httpResponseLayer->getFirstField(); headerField != nullptr; headerField = httpResponseLayer->getNextField(headerField))
                {
                    addChild(layerRoot, QString::fromStdString(headerField->getFieldName() + ": "), QString::fromStdString(headerField->getFieldValue()));
                }
            }
        }
    }

    static std::string getHttpMethod(pcpp::HttpRequestLayer::HttpMethod httpMethod)
    {
        switch (httpMethod)
        {
        case pcpp::HttpRequestLayer::HttpGET:
            return "GET";
        case pcpp::HttpRequestLayer::HttpHEAD:
            return "HEAD";
        case pcpp::HttpRequestLayer::HttpPOST:
            return "POST";
        case pcpp::HttpRequestLayer::HttpPUT:
            return "PUT";
        case pcpp::HttpRequestLayer::HttpDELETE:
            return "DELETE";
        case pcpp::HttpRequestLayer::HttpTRACE:
            return "TRACE";
        case pcpp::HttpRequestLayer::HttpOPTIONS:
            return "OPTIONS";
        case pcpp::HttpRequestLayer::HttpCONNECT:
            return "CONNECT";
        case pcpp::HttpRequestLayer::HttpPATCH:
            return "PATCH";
        default:
            return "Other";
        }
    }

    static std::string getHttpVersion(pcpp::HttpVersion httpVersion)
    {
        switch (httpVersion)
        {
        case pcpp::HttpVersion::ZeroDotNine:
            return "HTTP/0.9";
        case pcpp::HttpVersion::OneDotZero:
            return "HTTP/1.0";
        case pcpp::HttpVersion::OneDotOne:
            return "HTTP/1.1";
        default:
            return "Unknown Version";
        }
    }

    static void getDNSQueryOrResponse(uint16_t queryOrResponse, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (queryOrResponse)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(queryOrResponse, 0, 0), QString(" = Responce: Message is a query"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(queryOrResponse, 0, 0), QString(" = Responce: Message is a response"));
            break;
        }
    }

    static void getDNSOpcode(uint16_t opcode, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (opcode)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(opcode, 1, 4), QString(" = Opcode: Standard query (0)"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(opcode, 1, 4), QString(" = Opcode: Inverse query (1)"));
            break;
        case 2:
            addChild(flagRoot, strBinaryuint16_tdots(opcode, 1, 4), QString(" = Opcode: Status query (2)"));
            break;
        case 3:
            addChild(flagRoot, strBinaryuint16_tdots(opcode, 1, 4), QString(" = Opcode: Unasigned operation code (3)"));
            break;
        case 4:
            addChild(flagRoot, strBinaryuint16_tdots(opcode, 1, 4), QString(" = Opcode: Notify query (4)"));
            break;
        case 5:
            addChild(flagRoot, strBinaryuint16_tdots(opcode, 1, 4), QString(" = Opcode: Update query (5)"));
            break;
        default:
            addChild(flagRoot, strBinaryuint16_tdots(opcode, 1, 4), QString(" = Opcode: Operation code unknown"));
            break;
        }
    }

    static void getDNSAuthoriative(uint16_t authoritative, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (authoritative)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(authoritative, 5, 5), QString(" = Authoritiative: Server is not an authority for domain "));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(authoritative, 5, 5), QString(" = Authoritiative: Server is an authority for domain "));
            break;
        }
    }

    static void getDNSTruncation(uint16_t truncation, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (truncation)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(truncation, 6, 6), QString(" = Truncated: Message is not truncated"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(truncation, 6, 6), QString(" = Truncated: Message is truncated "));
            break;
        }
    }

    static void getDNSRecursionDesired(uint16_t recursionDesired, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (recursionDesired)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(recursionDesired, 7, 7), QString(" = Recursion desired: Do query non recursively"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(recursionDesired, 7, 7), QString(" = Recursion desired: Do query recursively"));
            break;
        }
    }

    static void getDNSRecursionAvailable(uint16_t recursionAvailable, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (recursionAvailable)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(recursionAvailable, 8, 8), QString(" = Recursion available: Server can do recursive queries"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(recursionAvailable, 8, 8), QString(" = Recursion available: Server can not do recursive queries"));
            break;
        }
    }

    static void getDNSZero(uint16_t zero, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (zero)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(zero, 9, 9), QString(" = Z: reserved (0)"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(zero, 9, 9), QString(" = Z: reserved (1)"));
            break;
        }
    }

    static void getDNSAnswerAuthenticate(uint16_t answerAuthenticate, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (answerAuthenticate)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(answerAuthenticate, 10, 10), QString(" = Anwser authenticated: Answer/authority portion was not authenticated by the server"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(answerAuthenticate, 10, 10), QString(" = Anwser authenticated: Answer/authority portion was authenticated by the server"));
            break;
        }
    }
    static void getDNSNonAuthData(uint16_t authData, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (authData)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(authData, 11, 11), QString(" = Non-authenticated data: Unacceptable"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(authData, 11, 11), QString("  = Non-authenticated data: Unacceptable"));
            break;
        }
    }

    static void getDNSReplyCode(uint16_t response, QTreeWidgetItem *flagRoot, pcpp::DnsLayer *dnsLayer)
    {
        switch (response)
        {
        case 0:
            addChild(flagRoot, strBinaryuint16_tdots(response, 12, 15), QString(" = Reply code: No error (0)"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint16_tdots(response, 12, 15), QString(" = Reply code: Format error (1)"));
            break;
        case 2:
            addChild(flagRoot, strBinaryuint16_tdots(response, 12, 15), QString(" = Reply code: Server Failure (2)"));
            break;
        case 3:
            addChild(flagRoot, strBinaryuint16_tdots(response, 12, 15), QString(" = Reply code: Non-existant domain (3)"));
            break;
        case 4:
            addChild(flagRoot, strBinaryuint16_tdots(response, 12, 15), QString(" = Reply code: Not implemented (4)"));
            break;
        case 5:
            addChild(flagRoot, strBinaryuint16_tdots(response, 12, 15), QString(" = Reply code: Query refused (5)"));
            break;
        default:
            addChild(flagRoot, strBinaryuint16_tdots(response, 12, 15), QString(" = Reply code: Response code unknown"));
            break;
        }
    }

    static std::string getDNSClass(pcpp::DnsClass dnsClass)
    {
        switch (dnsClass)
        {
        /* Internet class */
        case pcpp::DnsClass::DNS_CLASS_IN:
            return "IN";
        /* Internet class with QU flag set to True */
        case pcpp::DnsClass::DNS_CLASS_IN_QU:
            return "IN_QU";
        /* Chaos class */
        case pcpp::DnsClass::DNS_CLASS_CH:
            return "CH";
        /* Hesiod class */
        case pcpp::DnsClass::DNS_CLASS_HS:
            return "HS";
        /* ANY class */
        case pcpp::DnsClass::DNS_CLASS_ANY:
            return "ANY";
        default:
            return "Unknown class";
        }
    }

    static std::string getDNSType(pcpp::DnsType dnsType)
    {
        switch (dnsType)
        {
        /** IPv4 address record */
        case pcpp::DnsType::DNS_TYPE_A:
            return "A";
            /** Name Server record */
        case pcpp::DnsType::DNS_TYPE_NS:
            return "NS";
            /** Obsolete, replaced by MX */
        case pcpp::DnsType::DNS_TYPE_MD:
            return "MD";
        /** Obsolete, replaced by MX */
        case pcpp::DnsType::DNS_TYPE_MF:
            return "MF";
        /** Canonical name record */
        case pcpp::DnsType::DNS_TYPE_CNAME:
            return "CNAME";
        /** Start of Authority record */
        case pcpp::DnsType::DNS_TYPE_SOA:
            return "SOA";
        /** mailbox domain name record */
        case pcpp::DnsType::DNS_TYPE_MB:
            return "MB";
        /** mail group member record */
        case pcpp::DnsType::DNS_TYPE_MG:
            return "MG";
        /** mail rename domain name record */
        case pcpp::DnsType::DNS_TYPE_MR:
            return "MR";
        /** NULL record */
        case pcpp::DnsType::DNS_TYPE_NULL_R:
            return "NULL R";
        /** well known service description record */
        case pcpp::DnsType::DNS_TYPE_WKS:
            return "WKS";
        /** Pointer record */
        case pcpp::DnsType::DNS_TYPE_PTR:
            return "PTR";
        /** Host information record */
        case pcpp::DnsType::DNS_TYPE_HINFO:
            return "HINFO";
        /** mailbox or mail list information record */
        case pcpp::DnsType::DNS_TYPE_MINFO:
            return "MINFO";
        /** Mail exchanger record */
        case pcpp::DnsType::DNS_TYPE_MX:
            return "MX";
        /** Text record */
        case pcpp::DnsType::DNS_TYPE_TXT:
            return "TXT";
        /** Responsible person record */
        case pcpp::DnsType::DNS_TYPE_RP:
            return "RP";
        /** AFS database record */
        case pcpp::DnsType::DNS_TYPE_AFSDB:
            return "AFSDB";
        /** DNS X25 resource record */
        case pcpp::DnsType::DNS_TYPE_X25:
            return "X25";
        /** Integrated Services Digital Network record */
        case pcpp::DnsType::DNS_TYPE_ISDN:
            return "ISDN";
        /** Route Through record */
        case pcpp::DnsType::DNS_TYPE_RT:
            return "RT";
        /** network service access point address record */
        case pcpp::DnsType::DNS_TYPE_NSAP:
            return "NSAP";
        /** network service access point address pointer record */
        case pcpp::DnsType::DNS_TYPE_NSAP_PTR:
            return "PTR";
        /** Signature record */
        case pcpp::DnsType::DNS_TYPE_SIG:
            return "SIG";
        /** Key record */
        case pcpp::DnsType::DNS_TYPE_KEY:
            return "KEY";
        /** Mail Mapping Information record */
        case pcpp::DnsType::DNS_TYPE_PX:
            return "PX";
        /** DNS Geographical Position record */
        case pcpp::DnsType::DNS_TYPE_GPOS:
            return "GPOS";
        /** IPv6 address record */
        case pcpp::DnsType::DNS_TYPE_AAAA:
            return "AAAA";
        /**	Location record */
        case pcpp::DnsType::DNS_TYPE_LOC:
            return "LOC";
        /** Obsolete record */
        case pcpp::DnsType::DNS_TYPE_NXT:
            return "NXT";
        /** DNS Endpoint Identifier record */
        case pcpp::DnsType::DNS_TYPE_EID:
            return "EID";
        /** DNS Nimrod Locator record */
        case pcpp::DnsType::DNS_TYPE_NIMLOC:
            return "NIMLOC";
        /** Service locator record */
        case pcpp::DnsType::DNS_TYPE_SRV:
            return "SRV";
        /** Asynchronous Transfer Mode address record */
        case pcpp::DnsType::DNS_TYPE_ATMA:
            return "ATMA";
        /** Naming Authority Pointer record */
        case pcpp::DnsType::DNS_TYPE_NAPTR:
            return "NAPTR";
        /** Key eXchanger record */
        case pcpp::DnsType::DNS_TYPE_KX:
            return "KX";
        /** Certificate record */
        case pcpp::DnsType::DNS_TYPE_CERT:
            return "CERT";
        /** Obsolete, replaced by AAAA type */
        case pcpp::DnsType::DNS_TYPE_A6:
            return "A6";
        /** Delegation Name record */
        case pcpp::DnsType::DNS_TYPE_DNAM:
            return "DNAM";
        /** Kitchen sink record */
        case pcpp::DnsType::DNS_TYPE_SINK:
            return "SINK";
        /** Option record */
        case pcpp::DnsType::DNS_TYPE_OPT:
            return "OPT";
        /** Address Prefix List record */
        case pcpp::DnsType::DNS_TYPE_APL:
            return "APL";
        /** Delegation signer record */
        case pcpp::DnsType::DNS_TYPE_DS:
            return "DS";
        /** SSH Public Key Fingerprint record */
        case pcpp::DnsType::DNS_TYPE_SSHFP:
            return "SSHFP";
        /** IPsec Key record */
        case pcpp::DnsType::DNS_TYPE_IPSECKEY:
            return "IPSECKEY";
        /** DNSSEC signature record */
        case pcpp::DnsType::DNS_TYPE_RRSIG:
            return "RRSIG";
        /** Next-Secure record */
        case pcpp::DnsType::DNS_TYPE_NSEC:
            return "NSEC";
        /** DNS Key record */
        case pcpp::DnsType::DNS_TYPE_DNSKEY:
            return "DNSKEY";
        /** DHCP identifier record */
        case pcpp::DnsType::DNS_TYPE_DHCID:
            return "DHCID";
        /** NSEC record version 3 */
        case pcpp::DnsType::DNS_TYPE_NSEC3:
            return "NSEC3";
        /** NSEC3 parameters */
        case pcpp::DnsType::DNS_TYPE_NSEC3PARAM:
            return "NSEC3PARAM";
        /** All cached records */
        case pcpp::DnsType::DNS_TYPE_ALL:
            return "ALL";
        default:
            return "Unknown type";
        }
    }

    static const char *strBinaryuint16_tdots(uint16_t byte2, int start, int end)
    {
        static char buffer[17];
        int i = 0;

        while (i < start)
        {
            // putchar('.');
            buffer[i] = '.';
            i++;
        }

        uint16_t mask = 0x8000;
        mask >>= start;
        while (i <= end)
        {
            // putchar((byte2 & mask) ? '1': '0');
            buffer[i] = (byte2 & mask) ? '1' : '0';
            mask >>= 1;
            i++;
        }

        while (i <= 15)
        {
            // putchar('.');
            buffer[i] = '.';
            i++;
        }

        buffer[16] = '\0';
        return buffer;
    }
}