#include "packet_layer_transport.h"

namespace pkl

{
    void setTCPData(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::TCP))
        {
            pcpp::TcpLayer const *tcpLayer = packet.getLayerOfType<pcpp::TcpLayer>();
            if (tcpLayer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find TCP layer";
            }
            else
            {
                if (packetInfo != nullptr)
                {
                    auto *layerRoot = new QTreeWidgetItem(packetInfo);

                    addRoot(packetInfo, layerRoot, QString::fromStdString("Transmission Control Protocol"));
                    addChild(layerRoot, QString("Source Port: "), QString::number(tcpLayer->getSrcPort()));
                    addChild(layerRoot, QString("Destination Port: "), QString::number(tcpLayer->getDstPort()));
                    addChild(layerRoot, QString("Sequence Number: "), QString::number(tcpLayer->getTcpHeader()->sequenceNumber));
                    addChild(layerRoot, QString("Acknowledgment Number: "), QString::number(tcpLayer->getTcpHeader()->ackNumber));
                    addChild(layerRoot, QString("Header Length: "), QString::number(tcpLayer->getHeaderLen()));

                    auto *flagRoot = new QTreeWidgetItem(layerRoot);
                    addSubRoot(layerRoot, flagRoot, QString("Flags:"));

                    pkl::setTCPFlags(tcpLayer, flagRoot);
                    // QString fullFlagList;

                    // for (int i = 0; i < flags.size() + 3; i++)
                    // {
                    //     if (i <= 2)
                    //     {
                    //         fullFlagList.append("-");
                    //     }
                    //     else
                    //     {

                    //         if (flags.at(i - 3) == 1)
                    //         {

                    //             fullFlagList.append(flagNameLetters.at(i - 3));
                    //         }
                    //         else
                    //         {
                    //             fullFlagList.append("-");
                    //         }
                    //     }
                    // }
                    // fullFlagList.append("]");
                    // addChild(flagRoot, QString("[TCP Flags: "), fullFlagList);

                    addChild(layerRoot, QString("Window: "), QString::number(tcpLayer->getTcpHeader()->windowSize));
                    addChild(layerRoot, QString("Checksum: "), QString::number(tcpLayer->getTcpHeader()->headerChecksum));
                    addChild(layerRoot, QString("Urgent Pointer: "), QString::number(tcpLayer->getTcpHeader()->urgentPointer));
                }
            }
        }
    }

    void setTCPFlags(const pcpp::TcpLayer *tcpLayer, QTreeWidgetItem *flagRoot)
    {
        addChild(flagRoot, strBinaryuint12_tdots(0, 0, 3), QString(" = Reserved: Not Set"));

        switch (tcpLayer->getTcpHeader()->cwrFlag)
        {
        case 0:
            addChild(flagRoot, strBinaryuint12_tdots(0, 4, 4), QString(" = Congestion Window Reduced(CWR): Not Set"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint12_tdots(1, 4, 4), QString(" = Congestion Window Reduced(CWR): Set"));
            break;
        }

        switch (tcpLayer->getTcpHeader()->eceFlag)
        {
        case 0:
            addChild(flagRoot, strBinaryuint12_tdots(0, 5, 5), QString(" = ECN-Echo: Not Set"));
            break;
        case 1:
            break;
            addChild(flagRoot, strBinaryuint12_tdots(1, 5, 5), QString(" = ECN-Echo: Set"));
            break;
        }

        switch (tcpLayer->getTcpHeader()->urgFlag)
        {
        case 0:
            addChild(flagRoot, strBinaryuint12_tdots(0, 6, 6), QString(" = Urgent: Not Set"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint12_tdots(1, 6, 6), QString(" = Urgent: Set"));
            break;
        }

        switch (tcpLayer->getTcpHeader()->ackFlag)
        {
        case 0:
            addChild(flagRoot, strBinaryuint12_tdots(0, 7, 7), QString(" = Acknowledgment: Not Set"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint12_tdots(1, 7, 7), QString(" = Acknowledgment: Set"));
            break;
        }
        switch (tcpLayer->getTcpHeader()->pshFlag)
        {
        case 0:
            addChild(flagRoot, strBinaryuint12_tdots(0, 8, 8), QString(" = Push: Not Set"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint12_tdots(1, 8, 8), QString(" = Push: Set"));
            break;
        }
        switch (tcpLayer->getTcpHeader()->rstFlag)
        {
        case 0:
            addChild(flagRoot, strBinaryuint12_tdots(0, 9, 9), QString(" = Reset: Not Set"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint12_tdots(1, 9, 9), QString(" = Reset: Set"));
            break;
        }
        switch (tcpLayer->getTcpHeader()->synFlag)
        {
        case 0:
            addChild(flagRoot, strBinaryuint12_tdots(0, 10, 10), QString(" = Syn: Not Set"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint12_tdots(1, 10, 10), QString(" = Syn: Set"));
            break;
        }
        switch (tcpLayer->getTcpHeader()->finFlag)
        {
        case 0:
            addChild(flagRoot, strBinaryuint12_tdots(0, 11, 11), QString(" = Fin: Not Set"));
            break;
        case 1:
            addChild(flagRoot, strBinaryuint12_tdots(1, 11, 11), QString(" = Fin: Set"));
            break;
        }
    }

    void setUDPData(const pcpp::Packet &packet, QTreeWidget *packetInfo)
    {
        if (packet.isPacketOfType(pcpp::UDP))
        {
            pcpp::UdpLayer const *udpLayer = packet.getLayerOfType<pcpp::UdpLayer>();
            if (udpLayer == nullptr)
            {
                qDebug() << "Something went wrong, couldn't find UDP layer";
            }
            else
            {
                if (packetInfo != nullptr)
                {
                    auto *layerRoot = new QTreeWidgetItem(packetInfo);

                    addRoot(packetInfo, layerRoot, QString::fromStdString("User Datagram Protocol"));
                    addChild(layerRoot, QString("Source Port: "), QString::number(udpLayer->getSrcPort()));
                    addChild(layerRoot, QString("Destination Port: "), QString::number(udpLayer->getDstPort()));
                    addChild(layerRoot, QString("Length: "), QString::number(udpLayer->getHeaderLen()));
                    addChild(layerRoot, QString("Checksum: "), QString::number(udpLayer->getUdpHeader()->headerChecksum));
                }
            }
        }
    }

    static const char *strBinaryuint12_tdots(uint16_t byte2, int start, int end)
    {
        static char buffer[13];
        int i = 0;

        while (i < start)
        {
            // putchar('.');
            buffer[i] = '.';
            i++;
        }

        uint16_t mask = 0x8000;
        mask >>= start;
        while (i <= end)
        {
            // putchar((byte2 & mask) ? '1': '0');
            buffer[i] = (byte2 & mask) ? '1' : '0';
            mask >>= 1;
            i++;
        }

        while (i <= 11)
        {
            // putchar('.');
            buffer[i] = '.';
            i++;
        }

        buffer[12] = '\0';
        return buffer;
    }
}