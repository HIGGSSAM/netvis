#include "main_window.h"
#include "ui_main_window.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
                                          ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // sb_CurrentSource = new QLabel(this);
    // sb_CurrentDevice = new QLabel(this);
    // sb_PacketInfo = new QLabel(this);

    /* initialise mainwindow layout settings */
    init();
}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    writeAppSettings();
    event->accept();
}

/* initialise the mainwindow */
void MainWindow::init()
{
    /* register en::threadState type to Qt for use in signals */
    qRegisterMetaType<en::threadState>();

    auto val = en::threadState::err_devOpen;
    QVariant vt = QVariant::fromValue(val);
    en::threadState test = vt.value<en::threadState>();
    qDebug() << "err_devOpen" << (test == val);

    /* set state flags */
    isFileOpen = false;
    isFileSaved = true;
    isLiveCapture = false;
    isFileComment = false;
    isFileLoading = false;
    isFileSaving = false;
    filterBPF = new CaptureFilterBPF();
    sb_CurrentSource = new QLabel();
    sb_CurrentDevice = new QLabel();
    sb_PacketInfo = new QLabel();

    /* initialise session capture options */
    isShowComment = false;
    isShowAnalysis = false;
    stopAfterKBytes = 0;
    stopAfterPackets = 0;
    stopAfterSeconds = 0;
    captureFilter = nullptr;
    isAutoCaptureSave = false;
    autoCaptureFile = nullptr;

    /* restore app settings from config file/registry */
    readAppSettings();

    /* setup default device*/
    // setAvailableDevs();
    auto liveDevices = new CaptureLiveDevice();
    availableDevs = liveDevices->getAvailableDevices();

    // setDefaultDevInfo(QStringList());
    // set_sbCurrentDevice(defaultDev);

    auto liveDevice = new CaptureLiveDevice();
    availableDevs = liveDevice->getAvailableDevices();
    defaultDev = liveDevice->getDefaultDevice();

    qInfo() << "Available network interfaces: " << availableDevs;
    qInfo() << "Default network interface: " << defaultDev;

    /* setup status bar */
    createStatusBar();

    /* hude website menu item as not implemented */
    ui->actionWebsite->setVisible(false);

    /* set menu/toolbar controls initial state */
    setMenuButtons(appState::btn_init);

    // hide until file/live actions taken
    hide_sbPacketInfo();

    /* set help about app button with application name */
    ui->actionAbout->setText(tr("About ") + QApplication::applicationName());
    ui->actionAbout->setToolTip(tr("About ") + QApplication::applicationName());
    ui->actionAbout->setStatusTip(tr("About ") + QApplication::applicationName());

    /* set help website button with application name */
    ui->actionWebsite->setText(QApplication::applicationName() + tr(" website"));
    ui->actionWebsite->setToolTip(QApplication::applicationName() + tr(" website"));
    ui->actionWebsite->setStatusTip(QApplication::applicationName() + tr(" website"));
}

void MainWindow::setAvailableDevs()
{
    // /* get available devices */
    // const auto &devList = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDevicesList();
    // for (const auto &device : devList)
    // {
    //     // Dev structure:
    //     // 1] name 2] IPv4 3] Gateway 4] linklayer 5] ? IPv6

    //     openDev.clear();
    //     openDev.append(QString::fromStdString(device->getName()));
    //     openDev.append(QString::fromStdString(device->getIPv4Address().toString()));
    //     openDev.append(QString::fromStdString(device->getDefaultGateway().toString()));
    //     openDev.append(pkl::getLinkLayerName(device->getLinkType()));
    //     openDev.append("");

    //     /* check iterface name or if is not empty */
    //     if (QString::fromStdString(device->getIPv4Address().toString()) == "0.0.0.0" || QString::fromStdString(device->getIPv4Address().toString()) == "127.0.0.1")
    //     {
    //         continue;
    //     }
    //     availableDevs.append(openDev);
    // }
}

void MainWindow::setDefaultDevInfo(QStringList selectedDev)
{
    // /* selected device inputted */
    // if (!selectedDev.empty())
    // {
    //     defaultDev = selectedDev;
    //     return;
    // }

    // /* no selected device inputted defualt to the first in list of available devices */
    // if (!(availableDevs.first()).empty())
    // {
    //     defaultDev = availableDevs.first();
    //     return;
    // }
}
