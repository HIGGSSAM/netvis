#include "dialog_recent_files.h"
#include "ui_dialog_recent_files.h"

DialogRecentFiles::DialogRecentFiles(QWidget *parent)
    : QDialog(parent),
      ui(new Ui::DialogRecentFiles)
{
    ui->setupUi(this);
    readRecentFiles();
    selectedFile = nullptr;
}

DialogRecentFiles::~DialogRecentFiles()
{
    delete ui;
}

QString DialogRecentFiles::getFile() const
{
    return selectedFile;
}

void DialogRecentFiles::on_buttonBox_accepted()
{
    /* closes dialog box */
    if (ui->recentFilesList->currentItem()->foreground() == QColorConstants::LightGray)
    {
        qDebug() << "..file not found do nothing";
        selectedFile = nullptr;
    }
    else
    {
        selectedFile = ui->recentFilesList->currentItem()->text();
    }
}

void DialogRecentFiles::readRecentFiles() const
{
    QStringList fileList = AppSetting::getRecentFiles();
    for (int i = 0; i < fileList.size(); ++i)
    {
        if (QFile(fileList.at(i)).exists())
        {
            ui->recentFilesList->addItem(fileList.at(i));
        }
        else
        {
            ui->recentFilesList->addItem(fileList.at(i) + " (not found)");
            ui->recentFilesList->item(i)->font().setItalic(true);
            ui->recentFilesList->item(i)->setForeground(QColorConstants::LightGray);
        }
    }

    return;
}

void DialogRecentFiles::on_clearList_clicked(bool)
{
    qDebug() << "Clearlist";
    ui->recentFilesList->clear();
}

void DialogRecentFiles::on_recentFilesList_itemDoubleClicked(QListWidgetItem const *item)
{
    qDebug() << "Item double clicked: " << item->text();
}
void DialogRecentFiles::on_recentFilesList_itemClicked(QListWidgetItem const *item)
{
    qDebug() << "Item clicked" << item->text();
    // on_buttonBox_accepted();
}

void DialogRecentFiles::on_buttonBox_rejected()
{
    selectedFile = nullptr;
}
