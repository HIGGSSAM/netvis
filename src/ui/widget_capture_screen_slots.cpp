#include "widget_capture_screen.h"
#include "ui_widget_capture_screen.h"

int WidgetCaptureScreen::rendorArrowWidget(const pcpp::Packet *packet,
                                           int packetNumber, bool isDisplayPacket)
{
    arrow = new WidgetPacketArrow;
    connect(arrow, &WidgetPacketArrow::arrowClicked, this,
            &WidgetCaptureScreen::onArrowSelect);
    arrow->setClientDevDetails(defaultDev);
    arrow->setPacketNumber(packetNumber);

    /* create instance of raw packet from a parsed packet */
    pcpp::RawPacket const *rawPacket = packet->getRawPacket();
    arrow->setPacketLength(rawPacket->getRawDataLen());

    /* add packet comment */
    arrow->setUserComments(packetCommentList.at(packetNumber - 1).toStdString());

    long int sec = rawPacket->getPacketTimeStamp().tv_sec;
    long int nsec = rawPacket->getPacketTimeStamp().tv_nsec;

    if (pcpp::clockGetTime(sec, nsec) == 0)
    {
        long packetTime = ((double)rawPacket->getPacketTimeStamp().tv_sec) + (long double)(rawPacket->getPacketTimeStamp().tv_nsec / 1000000000.0);
        const char *dt = ctime(&packetTime);
        std::string str(dt);
        arrow->setPackerTimeStamp(str);
    }

    bool packetAdd = true;

    int lastLayer = 0;

    for (pcpp::Layer const *curLayer = packet->getFirstLayer(); curLayer != nullptr; curLayer = curLayer->getNextLayer())
    {
        switch (curLayer->getProtocol())
        {
        case pcpp::Ethernet:
            arrow->setLinkProtocol("Ethernet II");
            arrow->setForegroundColour("#000000");
            arrow->setBackgroundColour("#ffffff");
            lastLayer = 1;
            break;
        case pcpp::EthernetDot3:
            arrow->setLinkProtocol("IEEE 802.3 Ethernet");
            arrow->setForegroundColour("#000000");
            arrow->setBackgroundColour("#ffffff");
            break;
        case pcpp::ICMP:
            arrow->setTransportProtocol("ICMP"); // this is an internet protocol by is layered over IP4 or IP6
            arrow->setForegroundColour("#12272e");
            arrow->setBackgroundColour("#fce0ff");
            break;
        case pcpp::IPv4:
            arrow->setInternetProtocol("IPv4");
            getPacketArrowAddress(*packet, arrow);
            lastLayer = 2;
            break;
        case pcpp::IPv6:
            arrow->setInternetProtocol("IPv6");
            getPacketArrowAddress(*packet, arrow);
            lastLayer = 2;
            break;
        case pcpp::TCP:
            arrow->setTransportProtocol("TCP");
            arrow->setForegroundColour("#12272e");
            arrow->setBackgroundColour("#e7e6ff");
            lastLayer = 3;
            // add checks for flags for more colours
            break;
        case pcpp::UDP:
            arrow->setTransportProtocol("UDP");
            arrow->setForegroundColour("#12272e");
            arrow->setBackgroundColour("#daeeff");
            lastLayer = 3;
            break;
        case pcpp::SSL:
            arrow->setPresentationProtocol("TLS");
            lastLayer = 6;
            break;
        case pcpp::DNS:
            arrow->setApplicationProtocol("DNS");
            lastLayer = 7;
            break;
        case pcpp::HTTPResponse:
            arrow->setApplicationProtocol("HTTP");
            arrow->setForegroundColour("#12272e");
            arrow->setBackgroundColour("#e4ffc7");
            lastLayer = 7;
            break;
        case pcpp::HTTPRequest:
            arrow->setApplicationProtocol("HTTP");
            arrow->setForegroundColour("#12272e");
            arrow->setBackgroundColour("#e4ffc7");
            lastLayer = 7;
            break;
        case pcpp::UnknownProtocol:
            setUnknownProtocol(lastLayer, arrow);
            break;
        default:
            setUnknownProtocol(lastLayer, arrow);
        }
    }
    inner->layout()->addWidget(arrow);

    /* packet analysis threads */
    if (isShowAnalysis)
    {
        if (analyseHandshakes == nullptr)
        {
            analyseHandshakes = new CaptureAnalyseHandshakes();
            // add connections
            connect(analyseHandshakes, &CaptureAnalyseHandshakes::emitNewAnalysisComment,
                    this, &WidgetCaptureScreen::setAnalysisComment);
            //  connect(&CaptureAnalyseHandshakes, &CaptureAnalyseHandshakes::emitAnalysisFinished,
            //          this, &WidgetCaptureScreen::threadStatus);
            analyseHandshakes->setPacketPointer(packet);
            analyseHandshakes->startHandShakeAnalysis(packetNumber - 1);
            // createAnalysisThread(analyseHandshakes);
            //  arrow->setAnalysisComments(analyseHandshakes);
        }
        else
        {
            analyseHandshakes->setPacketPointer(packet);
            analyseHandshakes->startHandShakeAnalysis(packetNumber - 1);
            // arrow->setAnalysisComments(analyseHandshakes);
        }
    }

    /* set arrow's visibility */
    arrow->setVisible(isDisplayPacket);

    /* update session counters */
    packetsCaptured++;
    // parsedPacket
    bytesCaptured += rawPacket->getRawDataLen();
    // to add seconds counter

    /* test for session limit reached */
    if (stopAfterPackets != 0 && packetsCaptured >= stopAfterPackets)
    {
        if (isLiveCapture)
        {
            stopLiveCapture();
        }
        else
        {
            stopFileLoading();
        }
    }

    if (stopAfterSeconds != 0 && secondsCaptured >= stopAfterSeconds)
    {
        if (isLiveCapture)
        {
            stopLiveCapture();
        }
        else
        {
            stopFileLoading();
        }
    }

    if (stopAfterKBytes != 0 && (bytesCaptured / 1024) >= stopAfterKBytes)
    {
        if (isLiveCapture)
        {
            stopLiveCapture();
        }
        else
        {
            stopFileLoading();
        }
    }

    return 0;
}

void WidgetCaptureScreen::getPacketArrowAddress(const pcpp::Packet &packet, WidgetPacketArrow *arrow) const
{
    if (packet.isPacketOfType(pcpp::IPv4))
    {
        pcpp::IPv4Layer const *ip4Layer = packet.getLayerOfType<pcpp::IPv4Layer>();
        if (ip4Layer != nullptr)
        {
            arrow->setSrcAddress(ip4Layer->getSrcIPAddress().toString());
            arrow->setDstAddress(ip4Layer->getDstIPAddress().toString());
        }
    }
    if (packet.isPacketOfType(pcpp::IPv6))
    {
        pcpp::IPv6Layer const *ip6Layer = packet.getLayerOfType<pcpp::IPv6Layer>();
        if (ip6Layer != nullptr)
        {
            arrow->setSrcAddress(ip6Layer->getSrcIPv6Address().toString());
            arrow->setDstAddress(ip6Layer->getDstIPv6Address().toString());
        }
    }
}

int WidgetCaptureScreen::rendorPacketInfoWidget(const pcpp::Packet *packet)
{
    // clear tree widget down
    ui->treeWidget->clear();

    for (pcpp::Layer const *curLayer = packet->getFirstLayer(); curLayer != nullptr; curLayer = curLayer->getNextLayer())
    {
        switch (curLayer->getProtocol())
        {
        case pcpp::Ethernet:
            pkl::setEthernetData(*packet, ui->treeWidget);
            break;
        case pcpp::EthernetDot3:
            pkl::setEthernetDot3Data(*packet, ui->treeWidget);
            break;
        case pcpp::IPv4:
            pkl::setIPv4Data(*packet, ui->treeWidget);
            break;
        case pcpp::IPv6:
            pkl::setIPv6Data(*packet, ui->treeWidget);
            break;
        case pcpp::ICMP:
            pkl::setICMPData(*packet, ui->treeWidget);
            break;
        case pcpp::TCP:
            pkl::setTCPData(*packet, ui->treeWidget);
            break;
        case pcpp::UDP:
            pkl::setUDPData(*packet, ui->treeWidget);
            break;
        case pcpp::SSL:
            pkl::setTLSData(*packet, ui->treeWidget);
            break;
        case pcpp::DNS:
            pkl::setDNSData(*packet, ui->treeWidget);
            break;
        case pcpp::HTTPResponse:
            pkl::setHTTPResponseData(*packet, ui->treeWidget);
            break;
        case pcpp::HTTPRequest:
            pkl::setHTTPRequestData(*packet, ui->treeWidget);
            break;
        case pcpp::UnknownProtocol:
            break;
        default:
            return 1;
        }
    }
    return 0;
}

void WidgetCaptureScreen::setUnknownProtocol(int lastLayer, WidgetPacketArrow *arrow) const
{
    switch (lastLayer)
    {
    case 0:
        // unknown link
        arrow->setLinkProtocol("Unknown");
        break;
    case 1:
        // unknown internet
        arrow->setInternetProtocol("Unknown");
        arrow->setSrcAddress("Unknown");
        arrow->setDstAddress("Unknown");
        break;
    case 2:
        // unknown transport
        arrow->setTransportProtocol("Unknown");
    case 3:
        // unknown session/ presentation/ application
        arrow->setSessionProtocol("Unknown");
        arrow->setPresentationProtocol("Unknown");
        arrow->setApplicationProtocol("Unknown");
    case 4:
        // unknown presentation/ application
        arrow->setPresentationProtocol("Unknown");
        arrow->setApplicationProtocol("Unknown");
    case 5:
        // unknown application
        arrow->setApplicationProtocol("Unknown");
    default:
        break;
    }
}

int WidgetCaptureScreen::rendorPacketHexWidget(const pcpp::Packet *packet)
{
    /* clear raw packet text */
    ui->textBrowser->clear();

    /* create instance of raw packet from a parsed packet */
    pcpp::RawPacket const *rawPacket = packet->getRawPacket();

    QString rawDataText;

    char hexBuffer[3];
    for (int i = 0; i < rawPacket->getRawDataLen(); i++)
    {
        snprintf(hexBuffer, sizeof(hexBuffer), "%02X", ((uint8_t *)rawPacket->getRawData()[i]));
        rawDataText.append(hexBuffer);
        rawDataText.append(" ");
    }
    ui->textBrowser->append(rawDataText);
    return 0;
}

bool WidgetCaptureScreen::setSelectArrow(int arrowIndex, bool isSelected)
{
    if (isSelected)
    {
        // qDebug() << QString("Selecting arrow %1").arg(arrowIndex);
        /* highlight this selected packet */
        QLayoutItem *item = packetLayout->itemAt(arrowIndex);
        if (item == nullptr)
        {
            qCritical() << QString("Can not select arrow at %1 = null").arg(arrowIndex);
            return false;
        }
        WidgetPacketArrow *thisArrow = qobject_cast<WidgetPacketArrow *>(item->widget());
        thisArrow->selectPacket(true);

        /* move viewpoint of scroll area to show this arrow */
        ui->scrollArea->ensureWidgetVisible(thisArrow);

        /* set packet information */
        pcpp::Packet parsedPacket(rawPacketList.at(arrowIndex));
        rendorPacketInfoWidget(&parsedPacket);
        rendorPacketHexWidget(&parsedPacket);
    }
    else
    {
        // qDebug() << QString("De-selecting arrow %1").arg(arrowIndex);
        /* un-highlight previously selected packet */
        QLayoutItem *item = packetLayout->itemAt(arrowIndex);
        if (item == nullptr)
        {
            qCritical() << QString("Can not deselect arrow at %1 = null").arg(arrowIndex);
            return false;
        }
        WidgetPacketArrow *thisArrow = qobject_cast<WidgetPacketArrow *>(item->widget());
        thisArrow->selectPacket(false);
    }
    return true;
}
/*
    Sets arrow at position arrowIndex visible if isDisplay true, otherwise sets it hidden
*/
void WidgetCaptureScreen::displayArrow(int arrowIndex, bool isDisplay) const
{
    /* un-highlight previously selected packet */
    QLayoutItem *item = packetLayout->itemAt(arrowIndex);
    if (item == nullptr)
    {
        qCritical() << QString("Can not deselect arrow at %1 = null").arg(arrowIndex);
    }
    WidgetPacketArrow *thisArrow = qobject_cast<WidgetPacketArrow *>(item->widget());
    thisArrow->setVisible(isDisplay);
}

void WidgetCaptureScreen::onArrowSelect(int newPacketIndex)
{
    /* deselect only if there is a current selection */
    if (packetNumberFocusIndex >= 0)
        setSelectArrow(packetNumberFocusIndex, false);
    setSelectArrow(newPacketIndex, true);
    packetNumberFocusIndex = newPacketIndex;
}

void WidgetCaptureScreen::onArrowMove(arrowMove move)
{
    /* if no displayable packets do nothing */
    if (displayedPacketCnt == 0)
        return;

    int i;
    switch (move)
    {
    case arrowMove::next:
        /* find next displayed packet; if none stay on current packet */
        i = packetNumberFocusIndex + 1;
        while (i <= lastPacketNumberIndex)
        {
            if (display_hidePacketsList.at(i))
            {
                /* switch focus */
                onArrowSelect(i);
                // qDebug() << "moving to packet index " << i;
                break;
            }
            i++;
        }
        return;

    case arrowMove::previous:
        /* find previous displayed packet or if none stay on current packet */
        i = packetNumberFocusIndex - 1;
        while (i >= 0)
        {
            if (display_hidePacketsList.at(i))
            {
                /* switch focus */
                onArrowSelect(i);
                // qDebug() << "moving to packet index " << i;
                break;
            }
            i--;
        }
        return;

    case arrowMove::last:
        /* scan back from last packet to find next displayable packet */

        /* if already at last position do nothing */
        if (packetNumberFocusIndex == lastPacketNumberIndex)
            return;

        i = lastPacketNumberIndex;
        while (i >= 0)
        {
            if (display_hidePacketsList.at(i))
            {
                /* switch focus */
                onArrowSelect(i);
                // qDebug() << "moving to packet index " << i;
                break;
            }
            i--;
        }
        return;

    case arrowMove::first:
        /* scan back from first packet to find next displayable packet */

        /* if already at first position do nothing */
        if (packetNumberFocusIndex == 0)
            return;

        i = 0;

        while (i <= lastPacketNumberIndex)
        {
            if (display_hidePacketsList.at(i))
            {
                /* switch focus */
                onArrowSelect(i);
                // qDebug() << "moving to packet index " << i;
                break;
            }
            i++;
        }
        return;

    default:
        return;
    }
}

void WidgetCaptureScreen::setPacketDisplayFormat(arrowDisplay display) const
{
    /* loop over packets in arrow diagram */
    for (int i = 0; i <= lastPacketNumberIndex; i++)
    {
        QLayoutItem *item = packetLayout->itemAt(i);
        if (item == nullptr)
        {
            qCritical() << QString("Can not deselect arrow at %1 = null").arg(i);
            return;
        }

        WidgetPacketArrow *thisArrow = qobject_cast<WidgetPacketArrow *>(item->widget());

        switch (display)
        {
        case arrowDisplay::packetDefault:
            thisArrow->toggleAnalysisComments(false);
            thisArrow->toggleUserComments(false);
            break;
        case arrowDisplay::packetAnalysis:
            thisArrow->toggleAnalysisComments(true);
            thisArrow->toggleUserComments(false);
            break;
        case arrowDisplay::packetComments:
            thisArrow->toggleAnalysisComments(false);
            thisArrow->toggleUserComments(true);
            break;
        case arrowDisplay::packetAll:
            thisArrow->toggleAnalysisComments(true);
            thisArrow->toggleUserComments(true);
        }
    }
}

void WidgetCaptureScreen::setAnalysisComment(int index, const std::string &analysisComment) const
{

    QLayoutItem *item = packetLayout->itemAt(index);
    WidgetPacketArrow *thisArrow = qobject_cast<WidgetPacketArrow *>(item->widget());

    thisArrow->setAnalysisComments(analysisComment);
}
