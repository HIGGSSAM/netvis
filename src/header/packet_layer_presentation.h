#ifndef PACKET_LAYER_PRESENTATION_H
#define PACKET_LAYER_PRESENTATION_H

#include <iostream>

#include <QDebug>
#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <SSLLayer.h>
#include <Packet.h>
#include <SystemUtils.h>

#include "utils_packet_info_display.h"

/**
 * \namespace pkl
 * \brief Namespace for network packet layer functions
 *
 */
namespace pkl
{
    void setTLSData(const pcpp::Packet &packet, QTreeWidget *packetInfo);

    static void getTLSHandshakeHelloClientData(const pcpp::SSLHandshakeLayer *handshakeLayer,
                                               QTreeWidgetItem *handshakeItem);
    static void getTLSHandshakeServerClientData(const pcpp::SSLHandshakeLayer *handshakeLayer,
                                                QTreeWidgetItem *handshakeItem);
    static void getTLSHandshakeCertificate(const pcpp::SSLHandshakeLayer *handshakeLayer,
                                           const QTreeWidgetItem *handshakeItem);
    static void getTLSHandshakeCertStatus(pcpp::SSLHandshakeLayer *handshakeLayer,
                                          QTreeWidgetItem *handshakeItem);

    static std::string getTLSRecordType(pcpp::SSLRecordType recordType);
    static std::string getTLSHandshakeType(pcpp::SSLHandshakeType handshakeType);
    static std::string getTLSHandshakeExtensionType(pcpp::SSLExtensionType extType);
    static std::string getTLSAlertLevel(pcpp::SSLAlertLevel level);
    static std::string getTLSAlertDescription(pcpp::SSLAlertDescription description);
}
#endif // PACKET_LAYER_PRESENTATION_H