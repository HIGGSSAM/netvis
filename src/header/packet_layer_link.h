#ifndef PACKET_LAYER_LINK_H
#define PACKET_LAYER_LINK_H

#include <iostream>

#include <QDebug>
#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <EthLayer.h>
#include <EthDot3Layer.h>
#include <Packet.h>
#include <SystemUtils.h>

#include "utils_packet_info_display.h"
/**
 * @namespace pkl
 * @brief Namespace for network packet layer functions
 *
 */
namespace pkl
{
    /**
     * @brief Gets all the Ethernet II data from a packet, formats and displays them in a QTreeWidget.
     *
     * @param[in] packet        pointer to a parsed packet
     * @param[in] packetInfo    pointer to a QTreeWidget
     */
    void setEthernetData(const pcpp::Packet &packet, QTreeWidget *packetInfo);

    /**
     * @brief Gets all the IEEE 802.3 Ethernet data from a packet, formats and displays them in a QTreeWidget.
     *
     * @param[in] packet        pointer to a parsed packet
     * @param[in] packetInfo    pointer to a QTreeWidget
     */
    void setEthernetDot3Data(const pcpp::Packet &packet, QTreeWidget *packetInfo);

    const QHash<pcpp::LinkLayerType, QString> linkTypeName =
        {
            {pcpp::LINKTYPE_NULL, "BSD Loopback"},
            {pcpp::LINKTYPE_ETHERNET, "Ethernet"},
            {pcpp::LINKTYPE_AX25, "AX.25"},
            {pcpp::LINKTYPE_IEEE802_5, "IEEE 802.5"},
            {pcpp::LINKTYPE_ARCNET_BSD, "ARCHNET"},
            {pcpp::LINKTYPE_SLIP, "SLIP"},
            {pcpp::LINKTYPE_PPP, "PPP"},
            {pcpp::LINKTYPE_FDDI, "FDDI"},
            {pcpp::LINKTYPE_DLT_RAW1, "Raw IP"},
            {pcpp::LINKTYPE_DLT_RAW2, "Raw IP (OpenBSD"},
            {pcpp::LINKTYPE_PPP_HDLC, "PPP HDLC"},
            {pcpp::LINKTYPE_PPP_ETHER, "PPPoE"},
            {pcpp::LINKTYPE_ATM_RFC1483, "RFC 1483"},
            {pcpp::LINKTYPE_RAW, "Raw IP"},
            {pcpp::LINKTYPE_C_HDLC, "Cisco PPP HDLC"},
            {pcpp::LINKTYPE_IEEE802_11, "IEEE 802.11 wireless"},
            {pcpp::LINKTYPE_FRELAY, "Frame Relay"},
            {pcpp::LINKTYPE_LOOP, "OpenBSD loopback"},
            {pcpp::LINKTYPE_LINUX_SLL, "Linux cooked capture"},
            {pcpp::LINKTYPE_RAW, "Raw IP"},
            {pcpp::LINKTYPE_C_HDLC, "Cisco PPP HDLC"},
            {pcpp::LINKTYPE_IEEE802_11, "IEEE802_11"},
            {pcpp::LINKTYPE_FRELAY, "Frame Relay"},
            {pcpp::LINKTYPE_LOOP, "OpenBSD loopback"},
            {pcpp::LINKTYPE_LINUX_SLL, "LINUX_SLL"},
            {pcpp::LINKTYPE_LTALK, "Apple LocalTalk"},
            {pcpp::LINKTYPE_PFLOG, "OpenBSD pflog"},
            {pcpp::LINKTYPE_IEEE802_11_PRISM, "IEEE802_11_PRISM"},
            {pcpp::LINKTYPE_IP_OVER_FC, "IP-over-Fibre Channel"},
            {pcpp::LINKTYPE_SUNATM, "ATM traffic"},
            {pcpp::LINKTYPE_IEEE802_11_RADIOTAP, "Radiotap"},
            {pcpp::LINKTYPE_ARCNET_LINUX, "ARCNET"},
            {pcpp::LINKTYPE_APPLE_IP_OVER_IEEE1394, " Apple IP-over-IEEE 1394"},
            {pcpp::LINKTYPE_MTP2_WITH_PHDR, "MTP2_WITH_PHDR"},
            {pcpp::LINKTYPE_MTP2, "MTP2"},
            {pcpp::LINKTYPE_MTP3, "MTP3"},
            {pcpp::LINKTYPE_SCCP, "SCCP"},
            {pcpp::LINKTYPE_DOCSIS, "DOCSIS"},
            {pcpp::LINKTYPE_LINUX_IRDA, "LINUX_IRDA"},
            {pcpp::LINKTYPE_USER0, "USER0"},
            {pcpp::LINKTYPE_USER1, "USER1"},
            {pcpp::LINKTYPE_USER2, "USER2"},
            {pcpp::LINKTYPE_USER3, "USER3"},
            {pcpp::LINKTYPE_USER4, "USER4"},
            {pcpp::LINKTYPE_USER5, "USER5"},
            {pcpp::LINKTYPE_USER6, "USER6"},
            {pcpp::LINKTYPE_USER7, "USER7"},
            {pcpp::LINKTYPE_USER8, "USER8"},
            {pcpp::LINKTYPE_USER9, "USER9"},
            {pcpp::LINKTYPE_USER10, "USER10"},
            {pcpp::LINKTYPE_USER11, "USER11"},
            {pcpp::LINKTYPE_USER12, "USER12"},
            {pcpp::LINKTYPE_USER13, "USER13"},
            {pcpp::LINKTYPE_USER14, "USER14"},
            {pcpp::LINKTYPE_USER15, "USER15"},
            {pcpp::LINKTYPE_IEEE802_11_AVS, "EEE802_11_AVS"},
            {pcpp::LINKTYPE_BACNET_MS_TP, "BACNET_MS_TP"},
            {pcpp::LINKTYPE_PPP_PPPD, "PPP_PPPD"},
            {pcpp::LINKTYPE_GPRS_LLC, "GPRS_LLC"},
            {pcpp::LINKTYPE_GPF_T, "GPF_T"},
            {pcpp::LINKTYPE_GPF_F, "GPF_F"},
            {pcpp::LINKTYPE_LINUX_LAPD, "LAPD"},
            {pcpp::LINKTYPE_BLUETOOTH_HCI_H4, "HCI UART"},
            {pcpp::LINKTYPE_USB_LINUX, "Linux USB"},
            {pcpp::LINKTYPE_PPI, "PPI"},
            {pcpp::LINKTYPE_IEEE802_15_4, "IEEE802_15_4"},
            {pcpp::LINKTYPE_SITA, "SITA"},
            {pcpp::LINKTYPE_ERF, "ERF"},
            {pcpp::LINKTYPE_BLUETOOTH_HCI_H4_WITH_PHDR, "BLUETOOTH_HCI_H4_WITH_PHDR"},
            {pcpp::LINKTYPE_AX25_KISS, "AX25_KISS"},
            {pcpp::LINKTYPE_LAPD, "LAPD"},
            {pcpp::LINKTYPE_PPP_WITH_DIR, "PPP_WITH_DIR"},
            {pcpp::LINKTYPE_C_HDLC_WITH_DIR, "C_HDLC_WITH_DIR"},
            {pcpp::LINKTYPE_FRELAY_WITH_DIR, "FRELAY_WITH_DIR"},
            {pcpp::LINKTYPE_IPMB_LINUX, "IPMB_LINUX"},
            {pcpp::LINKTYPE_IEEE802_15_4_NONASK_PHY, "IEEE802_15_4_NONASK_PHY"},
            {pcpp::LINKTYPE_USB_LINUX_MMAPPED, "USB_LINUX_MMAPPED"},
            {pcpp::LINKTYPE_FC_2, "FC_2"},
            {pcpp::LINKTYPE_FC_2_WITH_FRAME_DELIMS, "FC_2_WITH_FRAME_DELIMS"},
            {pcpp::LINKTYPE_IPNET, "IPNET"},
            {pcpp::LINKTYPE_CAN_SOCKETCAN, "CAN_SOCKETCAN"},
            {pcpp::LINKTYPE_IPV4, "IPV4"},
            {pcpp::LINKTYPE_IPV6, "IPV6"},
            {pcpp::LINKTYPE_IEEE802_15_4_NOFCS, "IEEE802_15_4_NOFCS"},
            {pcpp::LINKTYPE_DBUS, "DBUS"},
            {pcpp::LINKTYPE_DVB_CI, "DVB_CI"},
            {pcpp::LINKTYPE_MUX27010, "MUX27010"},
            {pcpp::LINKTYPE_STANAG_5066_D_PDU, "STANAG_5066_D_PDU"},
            {pcpp::LINKTYPE_NFLOG, "NFLOG"},
            {pcpp::LINKTYPE_NETANALYZER, "NETANALYZER"},
            {pcpp::LINKTYPE_NETANALYZER_TRANSPARENT, "NETANALYZER_TRANSPARENT"},
            {pcpp::LINKTYPE_IPOIB, "IPOIB"},
            {pcpp::LINKTYPE_MPEG_2_TS, "MPEG_2_TS"},
            {pcpp::LINKTYPE_NG40, "NG40"},
            {pcpp::LINKTYPE_NFC_LLCP, "NFC_LLCP"},
            {pcpp::LINKTYPE_INFINIBAND, "INFINIBAND"},
            {pcpp::LINKTYPE_SCTP, "SCTP"},
            {pcpp::LINKTYPE_USBPCAP, "USBPCAP"},
            {pcpp::LINKTYPE_RTAC_SERIAL, "RTAC_SERIAL"},
            {pcpp::LINKTYPE_BLUETOOTH_LE_LL, "BLUETOOTH_LE_LL"},
            {pcpp::LINKTYPE_NETLINK, "NETLINK"},
            {pcpp::LINKTYPE_BLUETOOTH_LINUX_MONITOR, "BLUETOOTH_LINUX_MONITOR"},
            {pcpp::LINKTYPE_BLUETOOTH_BREDR_BB, "BLUETOOTH_BREDR_BB"},
            {pcpp::LINKTYPE_BLUETOOTH_LE_LL_WITH_PHDR, "BLUETOOTH_LE_LL_WITH_PHDR"},
            {pcpp::LINKTYPE_PROFIBUS_DL, "PROFIBUS_DL"},
            {pcpp::LINKTYPE_PKTAP, "PKTAP"},
            {pcpp::LINKTYPE_EPON, "EPON"},
            {pcpp::LINKTYPE_IPMI_HPM_2, "IPMI_HPM_2"},
            {pcpp::LINKTYPE_ZWAVE_R1_R2, "ZWAVE_R1_R2"},
            {pcpp::LINKTYPE_ZWAVE_R3, "ZWAVE_R3"},
            {pcpp::LINKTYPE_WATTSTOPPER_DLM, "WATTSTOPPER_DLM"},
            {pcpp::LINKTYPE_ISO_14443, "ISO_14443"}};
    /**
     * @brief Get the Link Layer Type
     *
     * @param LinkLayerName as QString
     * @return pcpp::LinkLayerType
     */
    pcpp::LinkLayerType getLinkLayerType(const QString &LinkLayerName);

    /**
     * @brief Gets the Link Layer Name
     *
     * @param LinkLayerType     as pcpp::LinkLayerType Enum
     * @return QString          Link layer name, or pcpp::LINKTYPE_ETHERNET if name not found
     */
    QString getLinkLayerName(pcpp::LinkLayerType LinkLayerType);
}

#endif // PACKET_LAYER_LINK_H