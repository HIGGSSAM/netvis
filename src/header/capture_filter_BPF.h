#ifndef CAPTURE_FILTER_BFP_H
#define CAPTURE_FILTER_BFP_H

#include <stdio.h>
#include <QString>
#include <PcapFileDevice.h>
#include <PcapFilter.h>

class CaptureFilterBPF
{

public:
    explicit CaptureFilterBPF();
    ~CaptureFilterBPF();

    /**
     * @brief Performs a check that the filter string is valid BPF syntax.
     * Valid filter string is stored for subsequent packet filter
     * matching.
     * @param[in] filter A QString filter to test if in valid BPF syntax
     * @return True if filter string is in a valid BPF syntax, false otherwise;
     */
    bool setFilter(const QString &filter);

    /**
     * @brief Tests whether the raw packet matches the filter which
     * must have been successfully checked viaa  setFilter() call.
     * @param[in] rawPacket A pointer to the raw packet to match the filter with
     * @return True if packet matches the filter, false otherwise;
     */
    bool matchPacketvsFilter(const pcpp::RawPacket *rawPacket);

    /**
     * @brief Returns the current filter.
     * @return The current filter (if valid) as QString, nullptr otherwise;
     */
    QString getFilter() const;

private:
    void init();
    QString filterString;
    pcpp::BpfFilterWrapper *filterObj;
};

#endif // CAPTURE_FILTER_BFP_H