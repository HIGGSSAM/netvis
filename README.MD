# NetVis

## General Information

---

NetVis is a GUI packet sniffer designed as a teaching tool to visualise network traffic in a diagrammatic representation. The application is designed with [Qt](https://www.qt.io/URL) and uses [PcapPlusPlus](https://pcapplusplus.github.io/) for packet capture.
The application has several key features:

* Allowing for both live capture and opening data files.
* Extended packet layer information.
* Filtering packets using the [Berkeley Packet Filter](<https://www.ibm.com/docs/en/qsip/7.4?topic=queries-berkeley-packet-filters>) syntax.
* Packet analysis of TCP and TLS handshakes.

## Installation

### Qt

You can download Qt from your devices package manager or from the Qt online Installer.

Qt download: <https://www.qt.io/download-qt-installer>

### PcapPlusPlus

You can download PcapPlusPlus through Homebrew or build it directly from source.

PcapPlusPlus download: <https://pcapplusplus.github.io/docs/install>

## Dependencies

Program | Version
--------|--------
Qt | > 5.15
PcapPlusPLus | == v22.05
CMake | > 3.5

## Notes

The following program was built and developed on:

* Debian GNU/Linux 11 (bullseye) running under Windows 11 wsl2.
* Ubuntu v20.04 lts running under Windows 11 wsl2.
